Spark Example
=============

このプロジェクトは [Spark](http://sparkjava.com) による Web アプリケーションの最小構成のサンプルです。
Java EE サーバ用に war ファイルにすることもできます。

動かし方
--------

アプリケーションとしても実行できますが、Jetty でも動かすことができます。

    gradle jettyEclipseRun

war の作り方
------------

以下のコマンドを実行すると build/libs フォルダに NowJava.war が作成されます。

    gradle build
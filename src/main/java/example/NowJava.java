package example;

import spark.servlet.SparkApplication;

import static spark.Spark.get;

/**
 * アプリケーションのエントリポイントを提供するクラスです。
 */
public class NowJava implements SparkApplication {
    public static void main(String[] argv) {
        new NowJava().init();
    }

    @Override
    public void init() {
        get("/hello", (request, response) -> "Hello World");
    }
}
